# K-Note Build Instructions

K-Note is an application utilizing a MERN stack to allow users to login to a private notes page.

You must have Node JS installed in order to run this application. Click this link to download: https://nodejs.org/en/download/

You can either clone this repository or download and unzip the source code. 

To run the application, cd into the correct directory, then type the command 'npm run initialStart' into the terminal.
This command installs all the dependencies and starts the application for you.
The project should automatically open in the browser. 