const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        min: 4,
        max: 10,
        trim: true,
    },
    password: {
        type: String,
        required: true,
        min: 4,
        max: 12,
        trim: true,
    },
    date_created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('User', userSchema);