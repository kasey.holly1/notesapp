const express = require('express');
const router = express.Router();
const User = require('../models/User');
const Joi = require('joi');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const schema = Joi.object({
    username: Joi.string().min(4).required(),
    password: Joi.string().min(4).required()
});

router.post('/register', async (req, res) => {
    
    const { error } = schema.validate(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    const userExist = await User.findOne({username: req.body.username});
    if (userExist) {
        return res.status(403).send('Username already exists');
    }

    // Encrpyt password
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);

    // Create new user
    const user = new User({
        username: req.body.username,
        password: hashPassword
    });
    try {
        const savedUser = await user.save();
        res.status(200).send(savedUser);
    } catch(err) {
        res.status(400).send(err);
    }
});

router.post('/login', async (req, res) => {
    const { error } = schema.validate(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    const userExist = await User.findOne({username: req.body.username});
    if (!userExist) {
        return res.status(401).send('User does not exist');
    }

    const validPass = await bcrypt.compare(req.body.password, userExist.password);
    if(!validPass) {
        return res.status(401).send('Incorrect password');
    }

    const token = jwt.sign({_id: userExist._id}, process.env.TOKEN_SECRET);
    res.header('auth-token', token);

    res.status(200).send({
        "status": "Logged in",
        "auth-token": token
    });
});

router.post('/password', async (req, res) => {
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(req.body.password, salt);
    
    try {
        await User.findByIdAndUpdate({"_id": req.body._id}, {"password": hashPassword});
        res.status(200).send("Success");
    } catch(err) {
        return res.status(400).send(err);
    }
});

module.exports = router;