const express = require('express');
const router = express.Router();
const verify = require('./verifyToken');
const Note = require('../models/Note');

router.get('/', async (req, res) => {
    const notes = await Note.find({userId: req.query.username});
    // if (notes.length == 0) {
    //     return res.status(200).send("User has no notes");
    // }

    res.status(200).send(notes);
});

router.post('/add', async (req, res) => {
    const note = new Note ({
        userId: req.body.userId,
        title: req.body.title,
        content: req.body.content,
        category: req.body.category
    });

    try {
        const savedNote = await note.save();
        res.status(200).send(savedNote);
    } catch(err) {
        return res.status(400).send(err);
    }
});

router.post('/remove', async (req, res) => {
    try {
        const response = await Note.deleteOne({"_id": req.query.id});
        res.status(200).send(response);
    } catch(err) {
        return res.status(400).send(err);
    }
});

router.post('/edit', async (req, res) => {
    const note = {
        title: req.body.title,
        content: req.body.content,
        category: req.body.category
    };

    try {
        const savedNote = await Note.findByIdAndUpdate({"_id": req.body._id}, note);
        res.status(200).send(savedNote);
    } catch(err) {
        return res.status(400).send(err);
    }
});

module.exports = router;