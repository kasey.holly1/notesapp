const express = require('express');
const app = express();
const connectDB = require('./config/db');
const cors = require('cors');
const PORT = 4000;

// Routes
const authRoute = require('./routes/auth');
const noteRoute = require('./routes/notes');
const verifyToken = require('./routes/verifyToken');

app.use(cors());
app.use(express.json());

connectDB();

app.use('/api/user', authRoute);
app.use('/api/notes', noteRoute); // verify needs to go here when it works

app.listen(PORT, function() {
    console.log("Server is running on Port: " + PORT);
});


