import React from 'react';
import './App.css';
import Login from './pages/Login';
import Home from './pages/Home';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import { CookiesProvider } from 'react-cookie';


export default function App() {
	
	return (
		<CookiesProvider>
			<BrowserRouter>
				<Switch>
					<Route exact path="/" component={Login}/>
					<Route exact path="/home" component={Home}/>
				</Switch>
			</BrowserRouter>
		</CookiesProvider>
	);
}

