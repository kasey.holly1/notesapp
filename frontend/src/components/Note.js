import React, { useState } from "react";
import { useForm } from "react-hook-form";
import "../styles/Notes.css";
import editLogo from "../logos/edit-interface-symbol.png";
import trashLogo from "../logos/trash-logo.png";

var randomColor = require('randomcolor');

export default function Note ( props ) {
    const {register, handleSubmit} = useForm();
    const [isDelete, setIsDelete] = useState(false);
    const [isEdit, setIsEdit] = useState(false);
    const [title, setTitle] = useState(props.title);
    const [category, setCategory] = useState(props.category);
    const [content, setContent] = useState(props.content);
    
    async function editNote (data) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        const newNote = {
            title: data.title,
            content: data.content,
            category: data.category,
            _id: props.id,
            userId: props.userId
        }

        var raw = JSON.stringify(newNote);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw
        };

        try {
            var response = await fetch("http://localhost:4000/api/notes/edit", requestOptions);
            if (response.status !== 200)
                throw "Error in edit note.";
            props.update();
            setIsEdit(false);        
        } catch(err) {
            console.log(err);
        }
    }
    
    async function deleteNote (id, update) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow'
        };
        
        try {
            const response = await fetch("http://localhost:4000/api/notes/remove/?id=" + id, requestOptions);
            console.log(response)
        } catch(err) {
            console.log(err);
        }

        update();
        setIsDelete(false);
    }

    return (
        <div id='note' style={{borderColor: randomColor()}}>
            <h2>{props.title}</h2>
            <div>Category: {props.category}</div>
            <p>{props.content}</p>
            <button id="edit-button" onClick={() => setIsEdit(true)}>
                <img id="edit-logo" src={editLogo} alt="edit" title="Edit Note"/>
            </button>
            <button id="delete-button" onClick={() => setIsDelete(true)}>
                <img id="delete-logo" src={trashLogo} alt="delete" title="Delete Note"/>
            </button>
            {
                isEdit &&
                    <form onSubmit={handleSubmit(editNote)}>
                        <input required id="title" type="text" name="title" ref={register} value={title} onChange={(event) => setTitle(event.target.value)}/>
                        <select name="category" ref={register} value={category} onChange={(event) => setCategory(event.target.value)}>
                            <option value="Notes">Notes</option>
                            <option value="To Do">To Do</option>
                            <option value="Password">Passwords</option>
                            <option value="List">List</option>
                            <option value="Other">Other</option>
                        </select>
                        <textarea required id="content" name="content" ref={register} value={content} onChange={(event) => setContent(event.target.value)}/>
                        <input type="submit" value={"Save"}/>
                        <button onClick={() => setIsEdit(false)}>Cancel</button>
                    </form>
            }
            {
                isDelete &&
                    <div>
                        <p>Are you sure you want to delete this note?</p>
                        <button onClick={() => deleteNote(props.id, props.update)}>Yes</button>
                        <button onClick={() => setIsDelete(false)}>Cancel</button>
                    </div>
            }
        </div>
    );
}