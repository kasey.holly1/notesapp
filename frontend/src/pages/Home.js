import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useCookies } from 'react-cookie';
import { Redirect } from 'react-router-dom';
import KLogo from "../logos/K.png"
import Note from "../components/Note.js"
import "../styles/Home.css"
const jwt = require("jsonwebtoken");

export default function Home () {
    const {register, handleSubmit} = useForm();
    const [isCreate, setIsCreate] = useState(false);
    const [isPassword, setIsPassword] = useState(false);
    const [notes, setNotes] = useState([]);
    const [confirmPass, setConfirmPass] = useState(false);
    const [passwordUpdated, setPasswordUpdated] = useState(false);
    const [cookies, setCookie, removeCookie] = useCookies(['authCookie']);
    const [redirect, setRedirect] = useState(false);

    useEffect(() => {
        getNotes();
    }, []);

    async function createNote(props) {
        var today = new Date();
        var username;

        try {
            var decoded = jwt.verify(cookies.authCookie, "jenfjfwifbi");
            username = decoded._id;
        } catch(err) {
            console.log(err);
            return <Redirect to='/'/>;
        }

        const newNote = {
            title: props.title,
            content: props.content,
            category: props.category,
            creationDate: today,
            userId: username
        }
        
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify(newNote);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        try {
            var response = await fetch("http://localhost:4000/api/notes/add", requestOptions);
            if (response.status !== 200)
                throw "Error in create note.";
            var temp = notes;
            temp.push(newNote);
            setNotes(temp);
            setIsCreate(false);        
        } catch(err) {
            console.log(err);
        }
    }

    async function update() {
        getNotes();
    }

    async function getNotes() {
        // Retrieve from DB
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        // console.log(cookies.authCookie);
        try {
            const verified = jwt.verify(cookies.authCookie,"jenfjfwifbi");
            var username = verified._id;
        } catch (error) {
            console.log(error);
            return <Redirect to='/'/>;
        }

        // Verify the cookie, if not valid, redirect to login
        // If valid, get the username out of it and pass in the fetch call

        var requestOptions = {
            method: 'GET',
            headers: myHeaders
        };

        try {
            const response = await fetch("http://localhost:4000/api/notes/?username=" + username, requestOptions);
            var data1 = await response.json();
            setNotes(Array.from(data1));
        } catch (error) {
            console.log(error);
        }
    }
    
    function sortNotes(sortType) {
        // Take Notes and Sort
        // alphabetical
        if (sortType == 'title') {
            const sorted = [...notes].sort((a,b) => {
                let la = a.title.toLowerCase(),
                    lb = b.title.toLowerCase();
                
                if (la < lb)
                    return -1;
                else if (la > lb)
                    return 1;
                else 
                    return 0;
            });
            console.log(sorted);
            setNotes(sorted);
        }
        // date created
        else if (sortType == 'date') {
            const sorted = [...notes].sort((a,b) => {
                let la = a.creationDate,
                    lb = b.creationDate;
                
                if (la < lb)
                    return -1;
                else if (la > lb)
                    return 1;
                else 
                    return 0;
            });
            console.log(sorted);
            setNotes(sorted);
        }
        // note type
        else {
            const sorted = [...notes].sort((a,b) => {
                let la = a.category.toLowerCase(),
                    lb = b.category.toLowerCase();
                
                if (la < lb)
                    return -1;
                else if (la > lb)
                    return 1;
                else 
                    return 0;
            });
            console.log(sorted);
            setNotes(sorted);
        }
    }

    function logout() {
        removeCookie('authCookie', { path: '/' });
        setRedirect(true);
    }

    function changePassword(data) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        if (data.password === data.confirmPassword) {
        
            try {
                const verified1 = jwt.verify(cookies.authCookie,"jenfjfwifbi");
                var userID = verified1._id;
            } catch (error) {
                console.log(error);
                return;
            }
            
            const data2 = {
                _id: userID,
                password: data.password
            };

            var raw = JSON.stringify(data2);

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw
            };

            fetch("http://localhost:4000/api/user/password", requestOptions)
            .then(response => response.text())
            .then(result => {
                console.log(result);
                setConfirmPass(false);
                setPasswordUpdated(true);
            })
            .catch(error => console.log('error', error));
        }

        else {
            setConfirmPass(true);
        }
    }

    if (redirect) {
        return <Redirect to='/'/>;
    }
    
    return (
        <div id='home-background'>
            <div id='header'>  
                <img src={KLogo} alt = "Logo" style={{display: 'inline-block', position: 'absolute', top: '0px', left: '0px'}}/>
                <h1 style={{width: '100%'}}>Your Notes</h1>
                <button id='sort-note' style={{display: 'inline-block', position: 'absolute', top: '20px', right: '5px'}} onClick={() => sortNotes('title')}>Sort Notes by Title</button>
                <button id='sort-note' style={{display: 'inline-block', position: 'absolute', top: '50px', right: '5px'}} onClick={() => sortNotes('date')}>Sort Notes by Creation Date</button>
                <button id='sort-note' style={{display: 'inline-block', position: 'absolute', top: '80px', right: '5px'}} onClick={() => sortNotes('category')}>Sort Notes by Category</button>
            </div> 
            <div id='notes'>
                {notes.map((note) => 
                    <Note 
                        title={note.title}
                        content={note.content}
                        category={note.category}
                        key={note._id}
                        id={note._id}
                        userId={note.userId}
                        update={update}
                    />
                )}
            </div>
            <div style={{marginBottom: "20px"}}>
                <button onClick={() => setIsCreate(true)} id='create-note' title='Create Note' style={{display: "block", marginLeft:"auto", marginRight:"auto"}}>+</button>
                <button onClick={() => setIsPassword(true)}>Change Password</button>
                <button onClick={() => logout()}>Logout</button>
            </div>
            {
                isCreate &&
                <form style={{margin:"10px"}} onSubmit={handleSubmit(createNote)}>
                        <input required id="title" type="text" name="title" ref={register} placeholder="Enter Title"/>
                        <select name="category" ref={register}>
                            <option value="Notes">Notes</option>
                            <option value="To Do">To Do</option>
                            <option value="Password">Passwords</option>
                            <option value="List">List</option>
                            <option value="Other">Other</option>
                        </select>
                        <textarea required id="content" name="content" ref={register} placeholder="Enter Content"/>
                        <input type="submit" value={"Save"}/>
                        <button onClick={() => setIsCreate(false)}>Cancel</button>
                    </form>
            }
            {
                isPassword &&
                <form id="password" onSubmit={handleSubmit(changePassword)}>
                    <input className='login' required type="password" ref={register} name="password" placeholder="Password" /><br/>
                    <input className='login' required type="password" ref={register} name="confirmPassword" placeholder="Confirm Password" /><br/><br/>
                    <input type="submit" value="CHANGE PASSWORD" /><br/>
                    {confirmPass && <div>Passwords do not match</div>}
                    {passwordUpdated && <div style={{fontSize: '75%', color: '#48b300'}}>Password sucessfully updated!</div>}
                    <button onClick={() => {setIsPassword(false); setConfirmPass(false)}}>Close</button>
                </form>
            }
        </div>
    );
}