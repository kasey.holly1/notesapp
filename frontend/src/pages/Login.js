import React, { useState } from 'react';
import { useForm } from "react-hook-form";
import { Redirect } from 'react-router-dom';
import { useCookies } from 'react-cookie';

import "../styles/Login.css"

export default function Login() {
    const {register, handleSubmit} = useForm();
    const [isLogin, setIsLogin] = useState(true);
    const [isNotValidText, setIsNotValidText] = useState("");
    const [confirmPass, setConfirmPass] = useState(false);
    const [userCreated, setUserCreated] = useState(false);
    const [cookies, setCookie] = useCookies(['authCookie']);
    const [redirect, setRedirect] = useState(false);
    const [repeatUser, setRepeatUser] = useState(false);
    

    async function login (data) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: JSON.stringify(data),
        };

        try {
            const response = await fetch("http://localhost:4000/api/user/login", requestOptions);

            // If Status Code == 200, Set Cookie and navigate to home page
            if (response.status === 200) {
                var data1 = await response.json();
                setCookie('authCookie', data1['auth-token'], { path: '/' });
                setRedirect(true);
            }
            
            // If Status Code == 401, Set Unauthorized
            else if (response.status === 401) {
                var data2 = await response.text();
                setIsNotValidText(data2);
                console.log(data2);
            }

            // Anything else, throw error
            else {
                throw new Error(response.status);
            }
        } catch(error) {
            console.log(error);
        }
    }

    if (redirect) {
        return <Redirect to='/home'/>;
    }

    async function createUser (data) {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        if (data.password === data.confirmPassword) {
            data = {
                username: data.username,
                password: data.password
            }
            var raw = JSON.stringify(data);

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };

            try {
                const response = await fetch("http://localhost:4000/api/user/register", requestOptions)
                if (response.status === 403) {
                    setRepeatUser(true);
                    setUserCreated(false);
                }
                else if (response.status === 200) {
                    const result = await response.json();
                    console.log(result);
                    setConfirmPass(false);
                    setUserCreated(true);
                    setRepeatUser(false);
                }
            } catch(error) {
                console.log(error);
            }
        }

        else {
            setConfirmPass(true);
        }
    }

    return (
        <div id="background">
            {/* login form */}
            { isLogin &&
                <form onSubmit={handleSubmit(login)}>
                    <h2 style={{color: '#636363'}}>K-NOTE SIGN IN</h2>
                    <input className='login' required type="text" name="username" ref={register} placeholder="Username" /><br/>
                    <input className='login' required type="password" name="password" ref={register} placeholder="Password" /><br/><br/>
                    <input type="submit" value={"SIGN IN"} />
                    <button onClick={() => {setIsLogin(false); setIsNotValidText("")}}>Create User</button>
                    { isNotValidText && <><div style={{color: 'red'}}>{isNotValidText}</div></>}
                </form>    
            }

            {/* create user form */}
            { !isLogin && 
                <form onSubmit={handleSubmit(createUser)}>
                    <h2 style={{color: '#636363'}}>K-NOTE SIGN UP</h2>
                    <input className='login' required type="text" ref={register} name="username" placeholder="Username" /><br/>
                    <input className='login' required type="password" ref={register} name="password" placeholder="Password" /><br/>
                    <input className='login' required type="password" ref={register} name="confirmPassword" placeholder="Confirm Password" /><br/><br/>
                    <input type="submit" value="CREATE ACCOUNT" /><br/>
                    {confirmPass && <div>Passwords do not match</div>}
                    {userCreated && <div style={{fontSize: '75%', color: '#48b300'}}>New user sucessfully created! Return to login.</div>}
                    {repeatUser && <div>User already exists.</div>}
                    
                    <button onClick={() => {setIsLogin(true); setUserCreated(false); setConfirmPass(false); setRepeatUser(false)}}>Back to Login</button>
                </form>
            }
        </div>
    );
}

